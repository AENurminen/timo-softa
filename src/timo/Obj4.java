/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class Obj4 extends ThirdClass{
 
     String Name;
            
    public Obj4(){
        
         Name="Laatikosto ";
         broken=false;
         
         size=125000;
         weight=25;
    }

    @Override
    public boolean isBroken() {
        return broken;
    }
    
    public String getName() {
        return Name;
    }
    
}
