/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;



/**
 *
 * @author Nurminen
 */
public class Thing {
    
    String name;
    int size;
    int weight;
     boolean broken;

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public Thing(String Name, int Size, int Weight , boolean Broken ) {
        
       name=Name;
       size=Size;
       weight=Weight;
       broken=Broken;
        
    }

    public boolean isBroken() {
        return broken;
    }    
    
    
    
    @Override
    public String toString(){
       return name; 
    }



}
