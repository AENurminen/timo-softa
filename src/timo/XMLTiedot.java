/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Nurminen
 */
public class XMLTiedot {

    private Document doc;
    private HashMap<String, String> map;
    ObservableList citys = FXCollections.observableArrayList();
    ObservableList citysAuto = FXCollections.observableArrayList();
    
    private ArrayList al=new ArrayList();
        
        
    
    public HashMap<String, String> getMap() {
        return map;
    }
    
    public ObservableList<String> getCitys() {
        return citys;
    }
    
    public ObservableList<String> getCitysAuto() {
        return citysAuto;
    }

    public XMLTiedot(String City) throws SAXException, MalformedURLException, IOException {
        URL url = new URL("http://smartpost.ee/fi_apt.xml");
         
         System.out.println("XMLTiedot 1");
         
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        
        String content = "";
        String line;
        
        
        while((line=br.readLine()) !=null){
            content += line + "\n";
            
        }

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            map = new HashMap();
            parseCurrentdata(City);

        } catch (ParserConfigurationException | IOException ex) {
            Logger.getLogger(XMLTiedot.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void parseCurrentdata(String City) {

        NodeList nodes = doc.getElementsByTagName("place");
        citysAuto.clear();
        
        SmartPostStorage st = new SmartPostStorage("SmartPostStorage");
        
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            
            
            String text = e.getElementsByTagName("city").item(0).getTextContent();
            String code = e.getElementsByTagName("code").item(0).getTextContent();
            String address = e.getElementsByTagName("address").item(0).getTextContent();
            String availability = e.getElementsByTagName("availability").item(0).getTextContent();
            String postoffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
            String lat = e.getElementsByTagName("lat").item(0).getTextContent();
            String lng = e.getElementsByTagName("lng").item(0).getTextContent();
            
            Float latFloat=Float.valueOf(lat);
            Float lngFloat=Float.valueOf(lng);
            
           
            
           SmartPost sp= new SmartPost( text, code, address, availability,  postoffice, latFloat, lngFloat);

          st.addSmartPost(sp);

      //  System.out.println("Terve Smartposteja tehdään");

       //  System.out.println(st.getSmartPosts().size());

        
           
           
            if(!citys.contains(text)){
           citys.add(text);
            }
            
            if(!citysAuto.contains(text)& City.equals(text)){
                
           citysAuto.add(postoffice);
            }
        }
        
        System.out.println("lprANTAAA");
        

        al = st.getSmartPosts();
        
        System.out.println(al.size());
    }

    private String getValue(String tag, Element e) {
        return ((Element) e.getElementsByTagName(tag).item(0)).getAttribute(tag);

    }
    
    public ArrayList<Packet> getSmartPosts(){
        return al;
    }

}
