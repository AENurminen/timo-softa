/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class Packet {
    
    protected int size; // uuden esineen
    protected int weight; // uuden esineen
   
    protected int speed;
    protected int distance;
    
    protected boolean broken;   // särkyvää
    
    
    String receiver;
    String Name;
    int Size;
    int Weight;
    Float lat;
    Float lng;
     Float lat2;
    Float lng2;
    
    int Pclass;

    public Packet() {

    }

    public Packet(String Receiver, String name, int size, int weight, Float Lat, Float Lng, Float Lat2, Float Lng2, boolean Broken, int Speed, int pclass) {
        Name = name;
        Size = size;
        Weight = weight;

        lat = Lat;
        lng = Lng;
        
         lat2 = Lat2;
        lng2 = Lng2;
        
        broken = Broken;
        speed=Speed;
        receiver=Receiver;
        Pclass=pclass;
    }

    public int getPclass() {
        return Pclass;
    }

    public Float getLat2() {
        return lat2;
    }

    public Float getLng2() {
        return lng2;
    }

    public String getReceiver() {
        return receiver;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLng() {
        return lng;
    }

    

    public boolean isBroken() {
        return broken;
    }

    public int getSpeed() {
        return speed;
    }

   

    public String getName() {
        return Name;
    }

    public int getSize() {
        return Size;
    }

    public int getWeight() {
        return Weight;
    }

    @Override
    public String toString() {
        return receiver;
    }

}
