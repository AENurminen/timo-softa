/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class SmartPost {
     String text;
            String code;
            String address;
            String availability;
            String postoffice;
            Float lat;
            Float lng;
            
    public  SmartPost(String Text,String Code,String Address, String Availability, String PostOffice,Float Lat,Float Lng){
    
       text=Text;
       code=Code;
       address=Address;
       availability=Availability;
       postoffice=PostOffice;
       lat=Lat;
       lng=Lng;
}

    public String getText() {
        return text;
    }

    public String getCode() {
        return code;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public Float getLat() {
        return lat;
    }

    public Float getLng() {
        return lng;
    }
    
     @Override
   public String toString(){
    return postoffice;
}
    
    
}
