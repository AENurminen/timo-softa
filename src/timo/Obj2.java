/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class Obj2 extends FirstClass{
    
     String Name;
            
    public Obj2(){
        
         Name="T-paitoja ";
         broken=false;
         
         size=2500;
         weight=1;
    }

    @Override
    public boolean isBroken() {
        return broken;
    }
    
    public String getName() {
        return Name;
    }
    
}
