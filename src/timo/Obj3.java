/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class Obj3 extends SecondClass{
    
     String Name;
            
    public Obj3(){
        
         Name="Lamppuja ";
         broken=true;
         size=4000;
         weight=1;
    }

     @Override
    public int getWeight() {
        return weight;
    }

    

     @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isBroken() {
        return broken;
    }
    
    public String getName() {
        return Name;
    }
}
