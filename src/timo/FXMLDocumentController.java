/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.awt.image.SampleModel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.xml.sax.SAXException;

/**
 *
 * @author Nurminen
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private WebView webview;

    @FXML
    private Label saaTiedot_label;

    @FXML
    private ComboBox<SmartPost> Citys_cb;
    @FXML
    private ComboBox<Packet> ChoosePacket_cb;

    public ArrayList al;
    
    private ArrayList sp;
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // webview.getEngine().load("https://uni.lut.fi/");
        webview.getEngine().load(getClass().getResource("index.html").toExternalForm());
        try {
            SetCombo();
        } catch (IOException | SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void addToMapAction(ActionEvent event) throws SAXException, IOException {
        
          

        SmartPostStorage Spstr= new SmartPostStorage("");
        Spstr.setSmartPosts(sp);
        
        SmartPost sp; 
                

            Float lat=Citys_cb.valueProperty().getValue().getLat();
            Float lng=Citys_cb.valueProperty().getValue().getLng();
            String address =Citys_cb.valueProperty().getValue().getAddress();
            String availability =Citys_cb.valueProperty().getValue().getAvailability();
            String city=Citys_cb.valueProperty().getValue().getText();
            String code =Citys_cb.valueProperty().getValue().getCode();
//            System.out.println( Citys_cb.valueProperty().getValue());
//           System.out.println( Citys_cb.valueProperty().getValue().getLat());
//             
           System.out.println(lat);
           System.out.println(lng);
           System.out.println(address);
           System.out.println(availability);
           
           String Address = "'"+address+", "+code+" "+city+"'";
           
           System.out.println(Address);
        //Citys_cb.valueProperty().getValue();

        webview.getEngine().executeScript("document.goToLocation(" + Address + ", 'Auki: 10-18'     , 'blue' )");

    //webview.getEngine().executeScript("document.goToLocation('Saaresjärventie 247, 17800 Kuhmoinen', 'Auki: 10-18'     , 'blue' )");
                 
    }

    private void getInfoToLabel(ActionEvent event) throws MalformedURLException, IOException, SAXException {

        String City ="Lahti";

//        City = Citys_cb.getValue();
//
//        System.out.println(Citys_cb.getValue());
        XMLTiedot w = new XMLTiedot(City);

        saaTiedot_label.setText((String) w.getMap().get("City") + "\n"
                + (String) w.getMap().get("Code") + "\n"
                + (String) w.getMap().get("Address") + "\n"
                + (String) w.getMap().get("Availability") + "\n"
                + (String) w.getMap().get("Name") + "\n"
                + (String) w.getMap().get("Lat") + "\n"
                + (String) w.getMap().get("Lng") + "\n"
        );

    }

    private void SetCombo() throws MalformedURLException, IOException, SAXException {

        String City="Valitse automaatti";
        //System.out.println(City);
        
        XMLTiedot w = new XMLTiedot(City);

        //Citys_cb.getItems().addAll(w.getCitys());
        
        
        sp=w.getSmartPosts();

        SmartPostStorage Spstr= new SmartPostStorage("");
        Spstr.setSmartPosts(sp);
        
        SmartPost sp; 
                
                 for (int i = 0; i < Spstr.getSmartPosts().size(); i++) {
            sp = Spstr.SmartPost(i);
            
            System.out.println( "Seur Citys getitems");
           
                
                Citys_cb.getItems().add(sp);
            
            
            
            
            
            System.out.println("Seuraava on ChoosePacket_cb.getValue().getName()");
            System.out.println(Citys_cb.getItems());
        }
                 
         
        
    }

    @FXML
    private void sendPacket_Action(ActionEvent event) {
        
        Float StartLat=ChoosePacket_cb.valueProperty().getValue().getLat();
        Float StartLng=ChoosePacket_cb.valueProperty().getValue().getLng();
        
        Float EndLat=ChoosePacket_cb.valueProperty().getValue().getLat2();
        Float EndLng=ChoosePacket_cb.valueProperty().getValue().getLng2();
        
        int speed=(int) ChoosePacket_cb.valueProperty().getValue().getSpeed();
        int PClass=ChoosePacket_cb.valueProperty().getValue().getPclass();
        
         System.out.println(StartLat);
         System.out.println(StartLng);
         
         System.out.println(EndLat);
         System.out.println(EndLng);
         
         System.out.println("seur speed");
         System.out.println(speed);
        
       // [aloitus lat, aloitus lon, lopetus lat, lopetus lon]
        
        ArrayList<Float> al = new ArrayList<>();

        al.add(StartLat);
        al.add(StartLng);
        al.add(EndLat);
        al.add(EndLng);
        
        Double distance= (Double) webview.getEngine().executeScript("document.createPath(" + al + ", 'red' , " + speed + ")");
        
         System.out.println("seur distance");
         System.out.println(distance);
        
        if(distance>150 && PClass==1 ){
            //webview.getEngine().executeScript("document.createPath(" + al + ", 'red' , " + speed + ")");
            webview.getEngine().executeScript("document.deletePaths()");
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Luokan 1 pakettia ei voida lähettää yli 150 km päähän!");
            alert.setHeaderText(null);
            alert.setContentText("Paketin lähetys peruutettiin!");
            alert.showAndWait();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Paketti lähetettiin onnistuneesti matkaan!");
            alert.setHeaderText(null);
            alert.setContentText("Lähetys onnistui!");
            alert.showAndWait();
        }

        
    }

    @FXML
    private void createNewPacket_Action(ActionEvent event) throws IOException {

//        FXMLLoader Loader = new FXMLLoader();
//        Loader.setLocation(getClass().getResource("Packet.fxml"));
//        try {
//            Loader.load();
//
//        } catch (IOException ex) {
//            Logger.getLogger(PacketController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        Parent p = Loader.getRoot();
//        Stage stage = new Stage();
//        stage.setScene(new Scene(p));
//        stage.setResizable(false);
//        //stage.getIcons().add(new Image(getClass().getResourceAsStream("OPO 4.0.png")));
//        //stage.setTitle("Muhkia: Osienpaikannusohjelma OPO");
//
//        stage.show();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Packet.fxml"));
        Scene home_page_scene = new Scene((Pane) loader.load());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
       
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(true);
        app_stage.setMaximized(false);

        PacketController controller = loader.<PacketController>getController();

        

        app_stage.show();
    }

    @FXML
    private void Remove_Action(ActionEvent event) {

        webview.getEngine().executeScript("document.deletePaths()");

    }

    public void setal(ArrayList AlPacketControllerista) {

         al = AlPacketControllerista;
       
        System.out.println("al size FXMLDocumentin puolella");
        System.out.println(al.size());
        
         Storage st = new Storage("Storage");
        st.setPackets(al);    // hakee FXMLDocumentControllerin privaatin ArrayList al:n ja asettaa sen Storageen
        System.out.println("al size FXMLDocumentin puolella");
        //System.out.println(al.size());

        Packet test = st.getPacket(0);

        System.out.println("Terve");
        //System.out.println(test.getName());

        for (int i = 0; i < st.getPackets().size(); i++) {
            test = st.getPacket(i);
            //System.out.println( st.getPacket(i));
            ChoosePacket_cb.getItems().add(test);
          //  System.out.println("Seuraava on ChoosePacket_cb.getValue().getName()");
           // System.out.println(ChoosePacket_cb.getItems());
        }

        //ChoosePacket_cb.getItems().addAll(st.getPacket(0));
       // System.out.println("Hello");
        //System.out.println(ChoosePacket_cb.getItems());
    }

    @FXML
    private void ChoosePac_Action(MouseEvent event) {

       // Storage st = new Storage("Storage");
       // st.setPackets(al);    // hakee FXMLDocumentControllerin privaatin ArrayList al:n ja asettaa sen Storageen
//        System.out.println("al size FXMLDocumentin puolella");
//        System.out.println(al.size());

//        Packet test = st.getPacket(0);
//
//        System.out.println("Terve");
//        System.out.println(test.getName());
//
//        for (int i = 0; i < st.getPackets().size(); i++) {
//            test = st.getPacket(i);
//            //System.out.println( st.getPacket(i));
//            ChoosePacket_cb.getItems().add(test);
//            System.out.println("Seuraava on ChoosePacket_cb.getValue().getName()");
//            System.out.println(ChoosePacket_cb.getItems());
//        }
//
//        //ChoosePacket_cb.getItems().addAll(st.getPacket(0));
//        System.out.println("Hello");
//        System.out.println(ChoosePacket_cb.getItems());

    }

}
